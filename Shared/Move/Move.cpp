#include "Move.h"

#include "GameObject/GameObject.h"
#include "GameObject/GameObjectFactory.h"
#include "GameObject/Vector3Component.h"

#include <vector>

namespace Engine
{
	namespace Move
	{
		struct Moveable
		{
			GameObject* pGameObject;
			Vector3Component* pGameObjectPosition;
			Vector3Component* pGameObjectForce;

			float kDrag;
			float Mass;
		};

		std::vector<Moveable*> Moveables;

		void AddMoveable(GameObject& NewGameObject, nlohmann::json& MoveableSection)
		{
			assert(MoveableSection["mass"].is_number_float());
			assert(MoveableSection["kd"].is_number_float());

			struct Moveable* pNewMoveable = new struct Moveable();

			pNewMoveable->Mass = MoveableSection["mass"];
			pNewMoveable->kDrag = MoveableSection["kd"];

			pNewMoveable->pGameObjectPosition = static_cast<Vector3Component*>(NewGameObject.EnsureComponent("Position", []()
				{
					return new Vector3Component("Position");
				}));

			pNewMoveable->pGameObjectForce = static_cast<Vector3Component*>(NewGameObject.EnsureComponent("Force", []()
				{
					return new Vector3Component("Force");
				}));

			Moveables.push_back(pNewMoveable);
		}

		void Init()
		{
			GameObjectFactory::RegisterComponentCreator("moveable", AddMoveable);
		}
	}
}