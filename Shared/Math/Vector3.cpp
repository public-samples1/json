#include "Vector3.h"

namespace Engine {
	const Vector3 Vector3::Zero(0.0f, 0.0f, 0.0f);
} // namespace Engine