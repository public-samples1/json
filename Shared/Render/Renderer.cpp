#include "Renderer.h"

#include "File/LoadFileToBuffer.h"
#include "GameObject/GameObject.h"
#include "GameObject/GameObjectFactory.h"
#include "GameObject/Vector3Component.h"

#include "GLib/GLib.h"

namespace Engine
{
	namespace Renderer
	{
		struct Renderable
		{
			GameObject* pGameObject;
			Vector3Component* pGameObjectPosition;

			GLib::Sprite* pSprite;
		};

		std::vector<Renderable*> Renderables;

		// forward declare
		GLib::Sprite* CreateSprite(const char* i_pFilename);
			
		void AddRenderable(GameObject& NewGameObject, nlohmann::json& RenderSection)
		{
			assert(RenderSection["sprite_texture_source"].is_string());

			std::string TextureSource = RenderSection["sprite_texture_source"];
			GLib::Sprite* pSprite = CreateSprite(TextureSource.c_str());

			if (pSprite)
			{
				Renderable* pNewRenderable = new struct Renderable();

				pNewRenderable->pGameObject = &NewGameObject;
				pNewRenderable->pGameObjectPosition = static_cast<Vector3Component*>(NewGameObject.EnsureComponent("Position", []()
					{
						return new Vector3Component("Position");
					})); 

				pNewRenderable->pSprite = pSprite;

				Renderables.push_back(pNewRenderable);
			}
		
		}

		void Init()
		{
			GameObjectFactory::RegisterComponentCreator("renderable", AddRenderable);
		}

		void Render()
		{
			for (Renderable* This : Renderables)
			{
				//GLib::Render(This->pSprite, This->pGameObjectPosition->get());e3ww4e3w
			}
		}

		GLib::Sprite* CreateSprite(const char* i_pFilename)
		{
			assert(i_pFilename);

			size_t sizeTextureFile = 0;

			// Load the source file (texture data)
			std::vector<uint8_t> TextureData = LoadFileToBuffer(i_pFilename);

			GLib::Sprite* pSprite = nullptr;

			// Ask GLib to create a texture out of the data (assuming it was loaded successfully)
			if (!TextureData.empty())
			{
				GLib::Texture* pTexture = GLib::CreateTexture(TextureData.data(), TextureData.size());

				unsigned int width = 0;
				unsigned int height = 0;
				unsigned int depth = 0;

				// Get the dimensions of the texture. We'll use this to determine how big it is on screen
				bool result = GLib::GetDimensions(*pTexture, width, height, depth);
				assert(result == true);
				assert((width > 0) && (height > 0));

				// Define the sprite edges
				GLib::SpriteEdges	Edges = { -float(width / 2.0f), float(height), float(width / 2.0f), 0.0f };
				GLib::SpriteUVs	UVs = { { 0.0f, 0.0f }, { 1.0f, 0.0f }, { 0.0f, 1.0f }, { 1.0f, 1.0f } };
				GLib::RGBA							Color = { 255, 255, 255, 255 };

				// Create the sprite
				pSprite = GLib::CreateSprite(Edges, 0.1f, Color, UVs, pTexture);

				// release our reference on the Texture
				GLib::Release(pTexture);
			}

			return pSprite;
		}
	}
}