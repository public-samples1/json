#pragma once

namespace Engine
{
	namespace Renderer
	{
		void Init();
		void Render();
	}
}