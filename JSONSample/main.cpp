

#include "PlayerController.h"

#include "GameObject/GameObject.h"
#include "GameObject/GameObjectFactory.h"
#include "Move/Move.h"
#include "Render/Renderer.h"


int main()
{
	using namespace Engine;

	Move::Init();
	Renderer::Init();

	GameObjectFactory::RegisterControllerCreator("player", &PlayerController::Create);

	GameObject* pNewGameObject = GameObjectFactory::CreateGameObject("data\\Player.json");
	return 0;
}
