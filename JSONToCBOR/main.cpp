#include "nlohmann/json.hpp"

#include <fstream>
#include <iostream>
#include <set>

int main(int i_argc, char** i_argl)
{
	using json = nlohmann::json;

	if (i_argc < 3)
	{
		std::cout << "Input and output file must be specified.";
		return -1;
	}

	std::ifstream InFile(i_argl[1]);

	json JSON = json::parse(InFile, nullptr, false);

	if (JSON.is_object())
	{
		// Example of validation
		// Ensure that the same component isn't in there twice.
		if (JSON.contains("components"))
		{
			assert(JSON["components"].is_object());

			std::set<std::string>	Components;

			for (json::iterator it = JSON["components"].begin(); it != JSON["components"].end(); ++it)
			{
				const std::string& ComponentName = it.key();

				if (Components.find(ComponentName) != Components.end())
					std::cout << "WARNING: Player already contains a component named " << ComponentName << "\n";
				else
					Components.insert(ComponentName);
			}
		}

		std::vector<uint8_t> CBOR = json::to_cbor(JSON);
		if (!CBOR.empty())
		{
			std::ofstream OutFile(i_argl[2], std::ios::out | std::ofstream::binary);

			std::copy(CBOR.begin(), CBOR.end(), std::ostreambuf_iterator<char>(OutFile));
		}
	}
	return 0;
}